## Assignment 49 OSPF routing protocol
/* By Per Dahlstroem. */
version 12.1X47-D15.4;
system {
    host-name R2;
    /* User: root Password: Rootpass */
    root-authentication {
        encrypted-password "$1$xH9xJoL6$MFOUYnZr4.Qj2NM24XInz/";
    }
}
interfaces {
    ge-0/0/1 {
        unit 0 {
            family inet {
                /* Lan10 */
                address 10.10.12.2/28;
            }
        }
    }
    ge-0/0/2 {
        unit 0 {
            family inet {
                /* Lan6 */
                address 10.10.11.1/28;
            }
        }
    }
    lo0 {
        unit 0 {
            family inet {
                address 192.168.100.2/32;
            }
        }
    }
}
protocols {
    ospf {
        area 0.0.0.0 {
            interface lo0;  /* Loopback interface */
            interface ge-0/0/1.0 {
                priority 1;            /*Set highest priority for DR election */
            }
            interface ge-0/0/2.0 {
                priority 1;              /* # Default priority for this interface */
            }
        }
    }
}
security {
    policies {
        /* This policy is needed for interfaceses in the R5_trust zone to intercommunicate */
        from-zone R5_trust to-zone R5_trust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
    }
    zones {
        security-zone R5_trust {
            interfaces {
                lo0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                        protocols {
                            ospf;
                        }
                    }
                }
                ge-0/0/1.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                        protocols {
            /* Open this interface up for participation in OSPF */
                            ospf;
                        }
                    }
                }
                ge-0/0/2.0 {
                    host-inbound-traffic {
                        system-services {
                            ping;
                        }
                        protocols {
            /* Open this interface up for participation in OSPF */
                            ospf;
                        }
                    }
                }
            }
        }
    }
}